from flask import json, request, current_app, jsonify, sessions
from sqlalchemy.orm import query
from app.models.certificate_model import CertificateModel



def create_certificate():
    data = request.json
    certificate = CertificateModel(**data)
    certificate.expirate_time()
    session = current_app.db.session
    session.add(certificate)
    session.commit()

    return jsonify(certificate), 201


def update_certificate():
    data = request.get_json()
    query = CertificateModel.query.filter_by(username=data['username']).one()
    CertificateModel.update_time(query)
    CertificateModel.update_all(query, data)
    session = current_app.db.session
    session.commit()

    return query.serialized, 200

def get_certificate():
    certificate = CertificateModel.query.all()
    return jsonify(certificate), 200


def delete_certificate():
    data = request.json
    certificate = CertificateModel.query.filter_by(username=data['username']).first()
    session = current_app.db.session
    session.delete(certificate)
    session.commit()

    return {'msg': 'Certificate deleted'}

    
from flask import Blueprint
from app.controllers.certificate_controller import create_certificate, delete_certificate, get_certificate, update_certificate


bp = Blueprint('certificate', __name__, url_prefix='/certificate')

bp.post('')(create_certificate) 
bp.patch('')(update_certificate)
bp.get('')(get_certificate)
bp.delete('')(delete_certificate)
from flask import Flask
from .certificate_blueprint import bp as bp_cert

def init_app(app:Flask):
    app.register_blueprint(bp_cert)
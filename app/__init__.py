from flask import Flask
from app.configs import database, env_configs, migration
from app import routes
from flask_cors import CORS

def create_app():
    
    app = Flask(__name__)
    CORS(app)
    env_configs.init_app(app)
    database.init_app(app)
    migration.init_app(app)
    routes.init_app(app)

    return app
from typing import List
from sqlalchemy.sql.sqltypes import DateTime, Integer
from app.configs.database import db
from sqlalchemy import String
from datetime import date, datetime, timedelta
from dataclasses import dataclass

@dataclass
class CertificateModel(db.Model):
    
    username: str
    name: str
    description: str
    groups: int
    expiration: int
    expirated_at: datetime
    created_at: datetime
    updated_at: datetime

    __tablename__ = 'certificate_table'

    id = db.Column(Integer, nullable=False, primary_key=True)
    username = db.Column(String(30), nullable=False, unique=True)
    name  = db.Column(String(255), nullable=False)
    description = db.Column(String)
    groups = db.Column(Integer)
    expiration = db.Column(Integer)
    expirated_at = db.Column(DateTime) 
    created_at = db.Column(DateTime, default=datetime.now())
    updated_at = db.Column(DateTime, nullable=True)


    @property
    def serialized(self):
        return {
            "username": self.username,
            "name": self.name,
            "description": self.description,
            "groups": self.groups,
            "expiration": self.expiration,
            "expirated_at": self.expirated_at,
            "created_at": self.created_at,
            "updated_at": self.updated_at
        }

    
    def expirate_time(self):
        self.expirated_at = datetime.now() + timedelta(self.expiration)


    def update_time(self):
        self.updated_at = datetime.today()

    def update_all(self, data):
        self.name = data['name']
        self.description = data['description']
        self.groups = data['groups']


    